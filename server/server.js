// dependencies
var redis = require("redis");
var express = require('express');

// config
var redis_host = "localhost";
var redis_port = "6379";
var redis_prefix = "cryptodo:list:";
var http_listen_port = 3000;

// database setup
console.log("Connecting to Redis server at " + redis_host + ":" + redis_port);

var db = redis.createClient(redis_port, redis_host, { retry_max_delay: 5000 });
db.on("error", function (err) {
	console.error("Redis: " + err);
});
db.on("ready", function () {
	console.log("Successfully connected to Redis database.");
});

// some useful wrappers

/** set object in database. calls cberr(key,msg) in case of errors */
function db_set_list(key, val, cberr) {
	if(!key || key.trim().length < 1) {
		console.log("db_set_list: Oops, empty key: " + key);
		cberr(key, "db_set_list: Oops, empty key?");
		return false;
	}
	db.set(redis_prefix + key, val);
	console.log("db_set_list: updated key " + key + " with value " + val);
}
/** get object from database. calls cb(key, val, errmsg). */
function db_get_list(key, cb) {
	console.log("db_get_list: hello");
	if(!key || key.trim().length < 1) {
		console.log("db_set_list: Oops, empty key: " + key);
		cb(key, null, "db_get_list: Oops, empty key?");
		return false;
	}
	console.log("db_get_list: pre-get " + redis_prefix + key);
	db.get(redis_prefix + key, function (err, reply) {
		console.log("db_get_list: reply is: " + reply);
		console.log("db_get_list: err is: " + err);
		if(reply && !err) {
			console.log("db_get_list: key found: " + key);
			cb(key, reply, null);
		} else {
			// key not found in database
			console.log("db_get_list: key not found: " + key);
			cb(key, null, err);
		}
	});
}

// route setup
var app = express();
app.use('/static', express.static(__dirname + '/../bower_components'));

app.get("/", function(req, res) {
	var body = "<html><head><title>CrypTodo</title></head><body>"+
		"<script type='text/javascript' src='/static/sjcl/sjcl.js'></script>" +
		"<script type='text/javascript' src='/static/jquery/dist/jquery.min.js'></script>" +
	//	"<script type='text/javascript'>sjcl.random.startCollectors();</script>" +
		"<script type='text/javascript'>function randomId() { return 'nine'; }</script>"+
		"<button onclick='window.location=\"/list/adc83b19e793491b1c6ea0fd8b46cd9f32e592fc\"' id='randomButton'>New random list</button>"+
		"<form action='#' onsubmit='window.location=\"/list/\"+document.getElementById(\"newlistname\").value; return false;'>"+
		"New list name: <input type='text' id='newlistname' />"+
		"<button type='submit'>OK</button>"+
		"<p id='progress'>no progress yet.</p>"+
		"<script>function setprogress() {"+
		" var s = (sjcl.random.getProgress() * 100) + '%, ' + (sjcl.random.isReady() ? 'finally ready!' : 'not ready yet');"+
		"	$('#progress').replaceWith('<p id=\"progress\">'+s+'</p>'); }"+
		"var i = setInterval('setprogress()', 1000);"+
		"sjcl.random.addEventListener('seeded', function(){ $('#progress').after('<p>READY!</p>');});"+
		"$('#progress').before('<p>hello world</p>');"+
		"sjcl.random.startCollectors();"+
		"</script></form></body></html>";
	res.send(body);
});

app.get("/list", function(req, res) {
	res.send("Which list do you want? Go back to the main page.");
});

app.get("/list/:id", function(req, res) {
	console.log("user asked for list " + req.params.id);
	db_get_list(req.params.id, function(key, val, err) {
		if(err) {
			res.send("you called for " + key + ", but there was an error: " + err);
		} else if(!err && !val) {
			res.send("you called for " + key + ", but that does not exist.");
		} else {
			res.send("you called for " + key + ", here it is: <br/>" + val);
		}
	});
});

app.get("/list/:id/set/:value", function(req, res) {
	console.log("user wants to update list " + req.params.id + " with " + req.params.value);
	var errors = false;
	db_set_list(req.params.id, req.params.value, function(key, err) {
		res.send("you called for " + key + ", but there was an error: " + err);
		errors = true;
	});
	if(!errors) {
		res.send("okay.");
	}
});

console.log("Listening on HTTP port " + http_listen_port);
app.listen(http_listen_port);

/* vim: set ts=2 sw=2 noet */
